import os 
import app
import unittest
import tempfile

class CatalogTestCase(unittest.TestCase):
    def test_home(self):
        rv = self.app.get('/')
        self.assertEqual(rv.status_code, 200)


if __name__ == '__main__':
    unittest.main()