from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/about')
def about():
  return '<h1> This is the about page  !  </h1>'


@app.route('/contact')
def contact():
  return '<h1> This is the Contact page  !  </h1>'


@app.route("/new")
def new():
  return " This is a new page !"


@app.route("/zizou")
def zizou():
  return " This is a zizou page !"